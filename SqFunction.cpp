#include "PCH.h"

SQFUNC(Sq_sqlite3_openDB)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if(iArgs != 1)
		return g_pApi->throwerror(vm, "(sqlite3_openDB) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_STRING)
		return g_pApi->throwerror(vm, "(sqlite3_openDB) wrong type of parameter 0, expecting 'string'");
		

	const SQChar *sDbName;
	g_pApi->getstring(vm, 2, &sDbName);

	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	rc = sqlite3_open(sDbName, &db);

	if(rc)
	{
		char buffer[1024];
		sprintf (buffer, "(sqlite3_openDB) Cannot open database: %s", sqlite3_errmsg(db));
		g_pApi->pushbool(vm, false);

		return g_pApi->throwerror(vm, buffer);
	}
	else
	{
		g_pApi->pushuserpointer(vm, db);

		return 1;
	}
}

SQFUNC(Sq_sqlite3_closeDB)
{
	int iArgs = g_pApi->gettop(vm) - 1;
	int info;

	if(iArgs != 1)
		return g_pApi->throwerror(vm, "(sqlite3_closeDB) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(sqlite3_closeDB) wrong type of parameter 0, expecting 'userpointer'");
	
	SQUserPointer pPointer;
	g_pApi->getuserpointer(vm, 2, &pPointer);

	sqlite3 *db = reinterpret_cast<sqlite3*>(pPointer);

	info = sqlite3_close(db);

	if(info == SQLITE_OK)
	{
		g_pApi->pushbool(vm, true);

		return 1;
	}
	else
	{
		g_pApi->pushbool(vm, false);

		return 1;
	};
}

SQFUNC(Sq_sqlite3_exec)
{
	char *zErrMsg = 0;
	int info;
	int iArgs = g_pApi->gettop(vm) - 1;

	if(iArgs != 2)
		return g_pApi->throwerror(vm, "(sqlite3_exec) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(sqlite3_exec) wrong type of parameter 0, expecting 'userpointer'");

	if (g_pApi->gettype(vm, 3) != OT_STRING)
		return g_pApi->throwerror(vm, "(sqlite3_exec) wrong type of parameter 1, expecting 'string'");

	const SQChar *sQuery;
	SQUserPointer pPointer;
	
	g_pApi->getuserpointer(vm, 2, &pPointer);
	g_pApi->getstring(vm, 3, &sQuery);

	sqlite3 *db = reinterpret_cast<sqlite3*>(pPointer);

	info = sqlite3_exec(db, sQuery, 0, 0, &zErrMsg);

	if(info != SQLITE_OK)
	{
		g_pApi->pushbool(vm, false);
		sqlite3_free(zErrMsg);
	}
	else
	{
		g_pApi->pushbool(vm, true);

		return 1;
	};

	return 1;
}

SQInteger eventStackSize;

void SqCallEventBegin(HSQUIRRELVM vm, const SQChar *name)
{
	eventStackSize = g_pApi->gettop(vm);

	g_pApi->pushroottable(vm);
	g_pApi->pushstring(vm, _SC("callEvent"), -1);

	if (SQ_SUCCEEDED(g_pApi->get(vm, -2)))
	{
		g_pApi->pushroottable(vm);
		g_pApi->pushstring(vm, name, -1);
	}
}

void SqCallEventEnd(HSQUIRRELVM vm)
{
	g_pApi->settop(vm, eventStackSize);

	eventStackSize = 0;
}

static int select_callback(void *data, int argc, char **argv, char **azColName)
{
	int i;

	SqCallEventBegin(g_pVM, "sqlite3_onSelect");

	g_pApi->pushstring(g_pVM, (SQChar*)data, -1);

	g_pApi->newtable(g_pVM);

	for (i = 0; i < argc; i++){
		g_pApi->pushstring(g_pVM, azColName[i], -1);
		g_pApi->pushstring(g_pVM, argv[i], -1);
		g_pApi->newslot(g_pVM, -3, SQFalse);
	}

	g_pApi->call(g_pVM, 4, SQTrue, SQFalse);

	SqCallEventEnd(g_pVM);
   
	return 0;
}

SQFUNC(Sq_sqlite3_errmsg)
{
	SQUserPointer pPointer;
	const SQChar *sError;

	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(sqlite3_errmsg) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(sqlite3_errmsg) wrong type of parameter 0, expecting 'userpointer'");

	g_pApi->getuserpointer(vm, 2, &pPointer);

	sqlite3 *db = reinterpret_cast<sqlite3*>(pPointer);

	sError = sqlite3_errmsg(db);

	g_pApi->pushstring(vm, sError, -1);

	return 1;
}

SQFUNC(Sq_sqlite3_errcode)
{
	SQUserPointer pPointer;
	SQInteger errcode;

	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(sqlite3_errcode) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(sqlite3_errcode) wrong type of parameter 0, expecting 'userpointer'");

	g_pApi->getuserpointer(vm, 2, &pPointer);

	sqlite3 *db = reinterpret_cast<sqlite3*>(pPointer);

	errcode = sqlite3_errcode(db);

	g_pApi->pushinteger(vm, errcode);

	return 1;
}

SQFUNC(Sq_sqlite3_errstr)
{
	SQInteger errcode;
	const SQChar *sStr;

	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(sqlite3_errstr) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(sqlite3_errstr) wrong type of parameter 0, expecting 'integer'");

	g_pApi->getinteger(vm, 2, &errcode);


	sStr = sqlite3_errstr(errcode);

	g_pApi->pushstring(vm, sStr, -1);

	return 1;
}

SQFUNC(Sq_sqlite3_execSelect)
{
	const SQChar *sQuery;
	const SQChar *sData;

	SQUserPointer pPointer;

	int iArgs = g_pApi->gettop(vm) - 1;

	char *zErrMsg = 0;
	int info;

	if(iArgs != 3) 
		return g_pApi->throwerror(vm, "(sqlite3_execSelect) wrong number of parameters, expecting 3");

	if (g_pApi->gettype(vm, 2) != OT_USERPOINTER)
		return g_pApi->throwerror(vm, "(sqlite3_execSelect) wrong type of parameter 0, expecting 'userpointer'");

	if (g_pApi->gettype(vm, 3) != OT_STRING)
		return g_pApi->throwerror(vm, "(sqlite3_execSelect) wrong type of parameter 1, expecting 'string'");

	if (g_pApi->gettype(vm, 4) != OT_STRING)
		return g_pApi->throwerror(vm, "(sqlite3_execSelect) wrong type of parameter 2, expecting 'string'");

	g_pApi->getuserpointer(vm, 2, &pPointer);
	g_pApi->getstring(vm, 3, &sData);
	g_pApi->getstring(vm, 4, &sQuery);

	sqlite3 *db = reinterpret_cast<sqlite3*>(pPointer);

	info = sqlite3_exec(db, sQuery, select_callback, (void*)sData, &zErrMsg);

	if(info != SQLITE_OK)
	{
		g_pApi->pushbool(vm, false);
		sqlite3_free(zErrMsg);
	}
	else
	{
		g_pApi->pushbool(vm, true);

		return 1;
	};

	return 1;
}