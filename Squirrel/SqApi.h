#pragma once

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#ifdef WIN32
	#define EXPORT __declspec(dllexport)
#elif defined(_GCC)
    #define EXPORT __attribute__((visibility("default")))
#else
	#define EXPORT
	#pragma warning Unknown dynamic link export semantics.
#endif

#define SQFUNC(x) SQInteger x(HSQUIRRELVM vm)