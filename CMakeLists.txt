cmake_minimum_required(VERSION 2.8.7)
project (SQLite)
set(CMAKE_BUILD_TYPE Release)

set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -m32")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_GLIBCXX_USE_CXX11_ABI=0")
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")

include_directories(Squirrel)
include_directories(sqlite)

file(GLOB SOURCES
        "dllmain.cpp"
        "PCH.h"
        "SqFunction.cpp"
        "SqFunction.h"
)

add_library(SQLite SHARED ${SOURCES})
target_link_libraries(SQLite ${CMAKE_SOURCE_DIR}/sqlite/sqlite3.o rt)