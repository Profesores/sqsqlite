#include "PCH.h"

HSQAPI g_pApi = NULL;
HSQUIRRELVM g_pVM = NULL;

void SqRegisterFunction(HSQUIRRELVM vm, const char *name, SQFUNCTION func)
{
	g_pApi->pushroottable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->newclosure(vm, func, 0);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

void SqRegisterEvent(HSQUIRRELVM vm, const SQChar *name)
{
	g_pApi->pushroottable(vm);
	g_pApi->pushstring(vm, _SC("addEvent"), -1);

	if (SQ_SUCCEEDED(g_pApi->get(vm, -2)))
	{
		g_pApi->pushroottable(vm);
		g_pApi->pushstring(vm, name, -1);

		g_pApi->call(vm, 2, SQFalse, SQTrue);
	}

	g_pApi->pop(vm, 2);
}

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _COLORS_
#define _COLORS_

	/* FOREGROUND */
#define RST  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define FRED(x) KRED x RST
#define FGRN(x) KGRN x RST
#define FYEL(x) KYEL x RST
#define FBLU(x) KBLU x RST
#define FMAG(x) KMAG x RST
#define FCYN(x) KCYN x RST
#define FWHT(x) KWHT x RST

#define BOLD(x) "\x1B[1m" x RST
#define UNDL(x) "\x1B[4m" x RST

#endif

SQRESULT EXPORT sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	#ifdef WIN32
	HANDLE hOut;
	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hOut, 11); //jasnobłękitny

	std::cout << "SQLite module for G2O v0.2" << std::endl;

	SetConsoleTextAttribute(hOut, 7);
	#else
	std::cout << FBLU("SQLite module for G2O v0.2") << std::endl;
	#endif


	g_pApi = api;
	g_pVM = vm;

	SqRegisterFunction(vm, "sqlite3_openDB", Sq_sqlite3_openDB);
	SqRegisterFunction(vm, "sqlite3_closeDB", Sq_sqlite3_closeDB);
	SqRegisterFunction(vm, "sqlite3_exec", Sq_sqlite3_exec);
	SqRegisterFunction(vm, "sqlite3_execSelect", Sq_sqlite3_execSelect);
	SqRegisterFunction(vm, "sqlite3_errmsg", Sq_sqlite3_errmsg);
	SqRegisterFunction(vm, "sqlite3_errcode", Sq_sqlite3_errcode);
	SqRegisterFunction(vm, "sqlite3_errstr", Sq_sqlite3_errstr);

	SqRegisterEvent(vm, "sqlite3_onSelect");

	return SQ_OK;
}

#ifdef __cplusplus
} /*extern "C"*/
#endif