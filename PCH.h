#pragma once

#ifdef WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include "sqlite3.h"
#include <stdlib.h>
#include <iostream>

#include "Squirrel/sqmodule.h"
#include "Squirrel/SqApi.h"

extern HSQAPI g_pApi;
extern HSQUIRRELVM g_pVM;

#include "SqFunction.h"