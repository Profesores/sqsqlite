#pragma once

SQFUNC(Sq_sqlite3_openDB);
SQFUNC(Sq_sqlite3_closeDB);
SQFUNC(Sq_sqlite3_exec);
SQFUNC(Sq_sqlite3_execSelect);
SQFUNC(Sq_sqlite3_errmsg);
SQFUNC(Sq_sqlite3_errcode);
SQFUNC(Sq_sqlite3_errstr);